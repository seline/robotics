/* This node controls the prius car based on people and obstacle detections
*/

#include <ros/ros.h>
#include <iostream>
#include <vision_msgs/Detection2DArray.h>
#include <vision_msgs/Detection3DArray.h>
#include <vision_msgs/Detection3D.h>
#include <prius_msgs/Control.h>

ros::Publisher pub;
bool stoppedDriving = false;	// if true, never start driving again


// Function definitions
void peopleMessageReceived(const vision_msgs::Detection2DArray& arraymsg);
void cloudMessageReceived(const vision_msgs::Detection3DArray& arraymsg);
void stopDriving();
double getDistance(vision_msgs::Detection3D detection);
vision_msgs::Detection3D getClosestPoint(std::vector<vision_msgs::Detection3D> detections);


int main(int argc, char **argv){
	// Initialize the ROS system
	ros::init(argc,argv, "control_solution_node");
	
	// Establisch program as a ROS node
	ros::NodeHandle nh;
	
	// Subscribe and publish
	ros::Subscriber subpeople = nh.subscribe("opencv_solution_node/detections",10,&peopleMessageReceived);
	ros::Subscriber subcloud = nh.subscribe("/pcl_solution_node/detections",100,&cloudMessageReceived);
	pub = nh.advertise<prius_msgs::Control>("/prius",100);
	
	// Give ROS control
	ros::spin();
}


// Stop driving if detected people are very close
void peopleMessageReceived(const vision_msgs::Detection2DArray& arraymsg){
	for(int ii=0; ii<arraymsg.detections.size(); ii++){
		double area = arraymsg.detections[ii].bbox.size_x * arraymsg.detections[ii].bbox.size_y;
		if(area > 25000){
			stopDriving();
			return;
		}
	}
}


// Stop driving and never start driving again
void stopDriving(){
	stoppedDriving = true;
	prius_msgs::Control controlmsg;
	controlmsg.throttle = 0.0;
	controlmsg.steer = 0;
	controlmsg.brake = 1.0;
	pub.publish(controlmsg);
}


// Control the car based on detected obstacles
void cloudMessageReceived(const vision_msgs::Detection3DArray& arraymsg){
	// Don't drive if a person has been too close
	if(stoppedDriving){
		return;
	}
	
	prius_msgs::Control controlmsg;
	
	// Filter 3D detections based on distance to car
	std::vector<vision_msgs::Detection3D> filteredDetections;
	for(int ii=0; ii<arraymsg.detections.size(); ii++){
		double x = arraymsg.detections[ii].bbox.center.position.x;
		// Keep only detections in front of car and within 6 meters
		if(x>0 && getDistance(arraymsg.detections[ii])<6.0){
			filteredDetections.push_back(arraymsg.detections[ii]);
		}
	}

	// Control algorithm
	controlmsg.throttle = 1.0;
	if(filteredDetections.empty()){
		controlmsg.steer = 0;
	}else{
		controlmsg.throttle = 1.0;
		// Steer left or right to avoid obstacles
		if(getClosestPoint(filteredDetections).bbox.center.position.y > 0){
			controlmsg.steer = -1;
		} else {
			controlmsg.steer = 1;
		}
	}
	
	// Publish the control message
	pub.publish(controlmsg);
}


// Get the distance between the car and a detection
double getDistance(vision_msgs::Detection3D detection){
	double x = detection.bbox.center.position.x;
	double y = detection.bbox.center.position.y;
	return sqrt(pow(x,2)+pow(y,2));
}


// Get the closest point to the car out of a vector of points
vision_msgs::Detection3D getClosestPoint(std::vector<vision_msgs::Detection3D> detections){
	int closestIndex = 0;
	double closestDistance = getDistance(detections[0]);
	for(int ii=1; ii<detections.size(); ii++){
		if(getDistance(detections[ii]) < closestDistance){
			closestIndex = ii;
			closestDistance = getDistance(detections[ii]);
		}
	}
	return detections[closestIndex];
}
