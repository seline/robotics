This project contains several nodes and package. Each with their own function, working towards simulating a self-driving car. 

First of all there is the opencv_solution package which contains the opencv_solution.cpp node. This node detects humans in a raw image and publishes them in a 2D array with the detections, which contain bounding boxes to surround the image in the simulation. 

Next there is the pcl_solution package. This contains a node that subscribes to the point cloud topic and uses this point cloud to detect objects (using the pcl library). These detection objects are then published as a 3D array to the topic /pcl_solution_node/detections.

Lastly the control_solution package combines both the object and human detections, to make the car avoid them and thus make the car self-driving. 

--------------------

To build the project you need to do the following:


1. Source
2. Create a ROS workspace called catkin_ws
3. Add src folder in this workspace and go to this folder
4. Clone the simulator and group25 repository to the src folder
5. Go back to the workspace and make the project using `catkin_make`
6. Source!!!
7. Launch the simple_control launch file

-----------------------

The following code should accomplish this:

```source /opt/ros/kinetic/setup.sh
cd 
mkdir -p catkin_ws/src
cd catkin_ws/src
catkin_init_workspace
git clone git@gitlab.3me.tudelft.nl:cor/me41025/students-1718/lab4/group25.git
git clone git@gitlab.3me.tudelft.nl:cor/me41025/simulator.git
cd ..
catkin_make
source devel/setup.sh
roslaunch control_solution solution.launch```
