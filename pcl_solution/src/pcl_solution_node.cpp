/* This program subscribe to the point cloud topic /point_cloud.
	Processes this received point cloud to find clusters.
	Publishes the clusters as a ROS topic (/pcl_solution_node/detections).
*/

#include <ros/ros.h>
#include <iostream>
#include <cmath>
#include <prius_msgs/Control.h>
#include <sensor_msgs/PointCloud2.h>
#include <vision_msgs/Detection3DArray.h>
#include <vision_msgs/BoundingBox3D.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>

ros::Publisher pub;

pcl::PointCloud<pcl::PointXYZ>::Ptr remove_gp(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud){
	/*This function removes the ground plane from the pointcloud.  First the segmentation is performed. And after the segmentation
		the ground plane components are extracted from the poincloud.
		INPUT: pointcloud (pointer) object
		OUTPUT: filtered pointcloud (pointer) object
		*/
	//Create the filtering object
  pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
	*pointcloud_filtered = *pointcloud;
	
	//Create the necessary pcl objects
	pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
		
	//Perform segmentation
	pcl::SACSegmentation<pcl::PointXYZ> seg;				// Create the segmentation object
	seg.setOptimizeCoefficients (true);				
	seg.setMethodType (pcl::SAC_RANSAC);
	seg.setModelType (pcl::SACMODEL_PLANE);
	seg.setDistanceThreshold (0.3);
	
	int i=0, nr_points = (int) pointcloud_filtered->points.size ();
  while (pointcloud_filtered->points.size () > 0.3 * nr_points)
  {
    // Segment the largest planar component from the remaining cloud
    seg.setInputCloud (pointcloud_filtered);
    seg.segment (*inliers, *coefficients);
    if (inliers->indices.size () == 0)
    {
      ROS_ERROR("Could not estimate a planar model for the given dataset.");
      break;
    }

    // Extract the planar inliers from the input cloud
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    extract.setInputCloud (pointcloud_filtered);
    extract.setIndices (inliers);

    // Remove the planar inliers, extract the rest
    extract.setNegative (true);
    extract.filter (*pointcloud_filtered);
  }
	return pointcloud_filtered;
}

vision_msgs::Detection3D create_detection(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster){
    //Create centroid and min, max objects for bounding box
    Eigen::Vector4f centroid;
    pcl::PointXYZ minPt, maxPt;
    
    //Compute the centroid and min and maximum
    pcl::compute3DCentroid(*cloud_cluster,centroid);	
    pcl::getMinMax3D(*cloud_cluster,minPt,maxPt);	
    
    //create detection3D object and define the centers and size of bbox
    vision_msgs::Detection3D detection;
    detection.bbox.center.position.x = centroid[0];
    detection.bbox.center.position.y = centroid[1];
    detection.bbox.center.position.z = centroid[2];
    
    detection.bbox.size.x = fabs(maxPt.x-minPt.x);
    detection.bbox.size.y = fabs(maxPt.y-minPt.y);
    detection.bbox.size.z = fabs(maxPt.z-minPt.z);
    
    return detection;
}



void pclMessageReceived(const sensor_msgs::PointCloud2& pclINmsg){
	/*------ Convert pointcloud to pcl pointcloud-----------*/

	//Create pcl message object
	pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud(new pcl::PointCloud<pcl::PointXYZ>);
	//Convert pointcloud message from sensor to pcl message
	pcl::fromROSMsg(pclINmsg, *pointcloud);
	//create message for ouput
	vision_msgs::Detection3DArray pclOUTmsg;	
	//Copy header to vision_msgs
	pclOUTmsg.header = pclINmsg.header;
	
	/*----------------Remove ground plane-----------------------*/
	pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);
	pointcloud_filtered = remove_gp(pointcloud);
	
	/*---------------------Cluster Extraction---------------------*/
	
	//pcl::PCDWriter writer;
	// Creating the KdTree object for the search method of the extraction
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
  tree->setInputCloud (pointcloud_filtered);
	
	std::vector<pcl::PointIndices> cluster_indices;  	//create vector with indices of clusters
  // Create Euclidean cluster extraction object and set parameters
  pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
  ec.setClusterTolerance (0.5); 
  ec.setMinClusterSize (10);
  ec.setMaxClusterSize (25000);
  ec.setSearchMethod (tree);
  ec.setInputCloud (pointcloud_filtered);
  ec.extract (cluster_indices);

	//Extract the clusters and save them in message
	for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
  {
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
    for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit) 
    cloud_cluster->points.push_back (pointcloud_filtered->points[*pit]); 
    cloud_cluster->width = cloud_cluster->points.size ();
    cloud_cluster->height = 1;
    cloud_cluster->is_dense = true;

    /*------------Converting and publishing the clusters---------------*/
    
    //Create detection message for current cluster
    vision_msgs::Detection3D detection;
    detection = create_detection(cloud_cluster);
    detection.header = pclINmsg.header;
    
    //Put the detection message of the cluster in the detection array message of all clusters
    pclOUTmsg.detections.push_back(detection);
  }
  
	//Publish message
	pub.publish(pclOUTmsg);
}

int main(int argc, char **argv){
	// Initialize the ROS system
	ros::init(argc,argv, "pcl_solution");
	// Establisch program as a ROS node
	ros::NodeHandle nh;
	//Display pcl solution is initialized
	ROS_INFO("Initialize pcl solution node");
	// Subscribe and publish
	ros::Subscriber sub = nh.subscribe("/point_cloud",100,&pclMessageReceived);
	pub = nh.advertise<vision_msgs::Detection3DArray>("/pcl_solution_node/detections",100);
	
	// Give ROS control
	ros::spin();
}



