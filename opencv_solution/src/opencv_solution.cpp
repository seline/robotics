/* This node detects people in a raw image and publishes the results as:
		- the raw image onto which the detections are drawn as rectangles
		- an array of 2D detections
*/

#include <ros/ros.h>
#include <iostream>
#include <sensor_msgs/Image.h>
#include <vision_msgs/Detection2DArray.h>
#include <vision_msgs/BoundingBox2D.h>
#include <vector>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h> // for converting ros image to opencv image
#include <image_transport/image_transport.h> // for publishing images

image_transport::Publisher pubvis;	// publisher for visualisation messages
ros::Publisher pubdet;							// publisher for detection array messages
cv::HOGDescriptor hog;							// people detector

// Function definitions
void imgMessageReceived(const sensor_msgs::Image& imgmsg);
void publishVisualisationMessage(cv_bridge::CvImagePtr cv_ptr, std::vector<cv::Rect> found);
void publishDetectionMessage(const sensor_msgs::Image& imgmsg, std::vector<cv::Rect> found);
vision_msgs::BoundingBox2D createBox(cv::Rect rect);



int main(int argc, char **argv){
	// Initialize the ROS system
	ros::init(argc,argv, "opencv_solution");
	// Establisch program as a ROS node
	ros::NodeHandle nh;
	
	// Subscribe to image topic
	ros::Subscriber sub = nh.subscribe("prius/front_camera/image_raw",1,&imgMessageReceived);	
	
	// Publish to visualisation topic
	image_transport::ImageTransport it(nh);
	pubvis = it.advertise("/opencv_solution_node/visual",1000);
	
	// Publish to detections topic
	pubdet = nh.advertise<vision_msgs::Detection2DArray>("/opencv_solution_node/detections",1000);
	
	// Create people detector
	hog.setSVMDetector(cv::HOGDescriptor::getDefaultPeopleDetector());
	
	// Give ROS control
	ros::spin();
}


// Detect people from raw image and publish results
void imgMessageReceived(const sensor_msgs::Image& imgmsg){
	// Tranform ros message into opencv image
	cv_bridge::CvImagePtr cv_ptr;
	cv_ptr = cv_bridge::toCvCopy(imgmsg);

	// Detect people
	std::vector<cv::Rect> found;
	hog.detectMultiScale(cv_ptr->image, found, 0, cv::Size(8,8));
	
	// Publish messages
	publishVisualisationMessage(cv_ptr, found);
	publishDetectionMessage(imgmsg, found);
}


// Publish visualisation message  (i.e. the raw image with rectangles drawn onto it)
void publishVisualisationMessage(cv_bridge::CvImagePtr cv_ptr, std::vector<cv::Rect> found){
	// Draw rectangles onto raw image
	for(int ii=0; ii<found.size(); ii++){
		rectangle(cv_ptr->image, found[ii], cv::Scalar(0.0,255.0,0,0),5);
	}
	
	// Publish raw image + rectangles to visualisation topic
  sensor_msgs::ImagePtr vismsg = cv_bridge::CvImage(std_msgs::Header(), "rgb8", cv_ptr->image).toImageMsg();
	pubvis.publish(vismsg);
}


// Publish detection message (i.e. the array of 2D detections)
void publishDetectionMessage(const sensor_msgs::Image& imgmsg, std::vector<cv::Rect> found){
	// Create empty detections array
	vision_msgs::Detection2DArray arraymsg;
	arraymsg.header = imgmsg.header;

	// Fill the array with the found detections
	for(int ii=0; ii<found.size(); ii++){
		vision_msgs::Detection2D detection;
		detection.bbox = createBox(found[ii]);
		arraymsg.detections.push_back(detection);
	}

	// Publish the array
	pubdet.publish(arraymsg);
}


// Transform opencv::Rect into vision_msgs::BoundingBox2D
vision_msgs::BoundingBox2D createBox(cv::Rect rect){
    vision_msgs::BoundingBox2D bbox;

    bbox.center.x = rect.x + rect.width/2.0;
    bbox.center.y = rect.y + rect.height/2.0;
    bbox.size_x = rect.width;
    bbox.size_y = rect.height;

    return bbox;
}
